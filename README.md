# Final Project JCC Batch 2 VueJS

This final project is a simple blog application using vuejs and vuetify.

## Group 14

1.  Farhan Haidar Rosadi (Kab. Sukabumi)

```
Email : fhrosadi@gmail.com
```

2. Lutfi Zain (Kota Bekasi)

```
Email : lutfidmz@gmail.com
```

3. Maria Artha Febriyanti Turnip (Kota Sukabumi)

```
Email : arthaturnip99@gmail.com
```

## Link Deployment Website

```
Link Website    : https://subtle-dodol-2a61e8.netlify.app/
```

## Link Demo Website

```

Link Video Demo : https://youtu.be/w4BxNyAn9rc

Link Google Drive : https://drive.google.com/drive/folders/1eyJaFLylkcgA6GU2jny4b-pBngmyQ6oU?usp=sharing
```

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Lints and fixes files

```
npm run lint
```
